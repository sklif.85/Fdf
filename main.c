
#include "Fdf.h"

int ft_count_points(char *tmp)
{
	char *s;
	int ndr_points;

	ndr_points = 0;
	s = tmp;
	while (*s)
	{
		if (*s != ' ')
			ndr_points++;
		s++;
	}
	return (ndr_points);
}


int ft_count_lines(char *av)
{
	int fd;
	int nbr_lines;
	char tmp;

	nbr_lines = 0;
	fd = 0;
	if ((fd = open(av, O_RDONLY) < 0))
		ft_fdf_error(2);
	while (read(fd, &tmp, 1))
		if (tmp == '\n')
			nbr_lines++;
	close(fd);
	return (nbr_lines);
}

void ft_parse_map(char **av, t_map **map, int fd)
{
	char *tmp;
	char **str;
	int x;
	int y;
	int l;

	l = 0;
	y = 0;
	*map = (t_map *)malloc(sizeof(t_map));
	//(*map)->line = NULL;
	//(*map)->len = 0;
	(*map)->line = (t_line **)malloc(sizeof(t_line *)/* * ft_count_lines(av[1])*/);
	if ((fd = open(av[0], O_RDONLY)) > 0)
	{
		while ((get_next_line(fd, &tmp)) > 0)
		{
			x = 0;
			(*(*map)->line) = (t_line *) malloc(sizeof(t_line));
			(*(*map)->line)->point = (t_point **) malloc(sizeof(t_point *) * ft_count_points(tmp));
			str = ft_strsplit(tmp, ' ');
			while (str[x])
			{
				(*(*(*map)->line)->point) = (t_point *) malloc(sizeof(t_point));
				(*(*(*map)->line)->point)->x = x;
				(*(*(*map)->line)->point)->y = l;
				(*(*(*map)->line)->point)->z = ft_atoi(str[x]);
				(*(*(*map)->line)->point)->r = 0;
				(*(*(*map)->line)->point)->g = 0;
				(*(*(*map)->line)->point)->b = 0;
				x++;
				(*(*map)->line)->point++;
			}
			(*(*(*map)->line)->point) = NULL;
			(*map)->line++;
			l++;
			free(str);
			free(tmp);
		}
		(*map)->line = NULL;
	}
}

int main(int ac, char **av)
{
	t_fdf *fdf;

	fdf = NULL;
	if (ac != 2)
		ft_fdf_error(1);
	fdf = (t_fdf *)malloc(sizeof(t_fdf));
	fdf->map = NULL;
	fdf->mlx = NULL;
	ft_parse_map(av, &fdf->map, 0);

}